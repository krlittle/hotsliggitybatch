# HotSliggityBatch
![#005B00](https://placehold.it/15/005B00/000000?text=+) HotSliggityBatch's <code>master</code> branch is working

[HotSliggityBatch](https://github.com/krlittle/HotSliggityBatch) is an app built using Spring Batch and utilizes Spring Web for making HTTP calls to a RESTful service.  

## Background
The intent of this project was to be a simple app that converts a text model into a request that could then be stored in a repository.  I chose to collect my data from hotslogs.com.  I wanted the process of collecting data to be as simple as possible so I just did a manual screenscrape by highlighting from the top-left corner of my match history to the bottom-right corner and pasted this into a local notepad document.  After deciding this would be my repetitive text data model, I configured the batch application from there.

## Configuration
This project is currently configured to point to [HotSliggitySlogs](https://github.com/krlittle/HotSliggitySlogs) and starts up on port 8083 by default.  To change these configurations, please refer to the <code>[application.properties](https://github.com/krlittle/HotSliggityBatch/blob/master/src/main/resources/application.properties)</code> file.

## How to run
<ol>
<li>Create four files named hotsliggity_quickmatch.txt, hotsliggity_heroleague.txt, hotsliggity_teamleague.txt, hotsliggity_unrankeddraft.txt</li>
<li>Copy their location and update the <code>input.resources</code> property in the <code>application.properties</code> file</li>
<li>Make certain you have your MongoDB instance running</li>
<li>Make certain you have the HotSliggitySlogs app running on port 8081 (or the port you configured in the <code>application.properties</code> file)</li>
<li>Make certain port 8083 (or the port you configured in the <code>application.properties</code> file) is open</li>
<li>Run <code>HotSliggityBatchApp.java</code></li>
<li>You will see output written to your console</li>
</ol>
